from django.db import models
from predio.models import Predio


# Create your models here.
class Propietario(models.Model):
    cedula_ciudadania = models.IntegerField(unique=True, primary_key=True, verbose_name='cedula ciudadania')
    nombre = models.CharField(max_length=128)
    matricula_inmobiliaria = models.ManyToManyField(Predio,blank=True)


    class Meta:
        verbose_name = "propietario"
        verbose_name_plural = "propietarios"


    def __str__(self):
        return '{}{}{}'.format(self.cedula_ciudadania, self.nombre, self.matricula_inmobiliaria)

