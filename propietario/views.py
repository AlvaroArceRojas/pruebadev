from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from propietario.forms import PropietarioForm
from propietario.models import Propietario
from django.shortcuts import render
from .filters import PropietarioFilter


# Create your views here.


def search(request):
    propietario_filtrar = Propietario.objects.all(cedula_ciudadania__icontains=request.Get.buscar, nombre__icontains= request.Get.buscar )
    propietario_filter = Propietario(request.GET, queryset= propietario_filtrar)
    return render(request, 'propietario/propietario_listar.html', {'filter': propietario_filter})


class PropietarioList(ListView):
    model = Propietario
    template_name = 'propietario/propietario_listar.html'


class PropietarioCreate(CreateView):
    model = Propietario
    form_class = PropietarioForm
    template_name = 'propietario/propietario_form.html'
    success_url = reverse_lazy('propietario:propietario_listar')

class PropietarioUpdate(UpdateView):
    model = Propietario
    form_class = PropietarioForm
    template_name = 'propietario/propietario_form.html'
    success_url = reverse_lazy('propietario:propietario_listar')

class PropietarioDelete(DeleteView):
    model = Propietario
    template_name = 'propietario/propietario_delete.html'
    success_url = reverse_lazy('propietario:propietario_listar')