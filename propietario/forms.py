from django import forms
from propietario.models import Propietario

class PropietarioForm(forms.ModelForm):
    class Meta:
        model = Propietario

        fields = [
            'cedula_ciudadania',
            'nombre',
            'matricula_inmobiliaria',
         ]
        # labels = {
        #     'cedula_ciudadania': 'Cedula de Ciudadania Propietario',
        #     'nombre': 'Nombre Propietario',
        # }
        # widgets = {
        #     'cedula_ciudadania':forms.TextInput(attrs={'class':'form-control'}),
        #     'nombre': forms.TextInput(attrs={'class':'form-control'}),
        # }