from django.urls import path
from . import views
from .views import Propietario, PropietarioForm, PropietarioList, PropietarioCreate, PropietarioDelete, PropietarioUpdate

app_name = 'propietario'
urlpatterns = [
    path('search', views.search, name='propietario_filter'),
    path('nuevo', PropietarioCreate.as_view(), name='propietario_crear'),
    path('listar', PropietarioList.as_view(), name= 'propietario_listar'),
    path('editar/<int:pk>', PropietarioUpdate.as_view(), name= 'propietario_editar'),
    path('eliminar/<int:pk>', PropietarioDelete.as_view(), name= 'propietario_eliminar'),
]