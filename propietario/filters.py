from propietario.models import Propietario

class PropietarioFilter:

    class Meta:
        model = Propietario
        fields = ['cedula_ciudadania', 'nombre']