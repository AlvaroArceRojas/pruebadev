from django import forms
from predio.models import Predio


class PredioForm(forms.ModelForm):
    class Meta:
        model = Predio


        fields = [
            'matricula_inmobiliaria',
            'cedula_catastral',
            'tipo',
            'direccion_predio',
            'nombre_predio',
            ]

        def __init__(self, *args, **kwargs):
            super(PredioForm, self).__init__(*args, **kwargs)
            if 'tipo' == 'urbano':
                self.fields['nombre_predio'].disabled = True
            else:
                self.fields['direccion_predio'].disabled = True
        # if 'tipo' == 'urbano':
        #     prop('nombre_predio')
        # else:
        #     fields.exclude('direccion_predio')

       # labels = {
       #      'matricula_inmobiliaria': 'matricula inmobiliaria',
       #      'cedula_catastral': 'cedula catastral',
       #      'tipo' : 'tipo',
       #      'direccion_predio': 'direccion predio',
       #      'nombre_predio' : 'nombre predio',
       #  }
       #
       #  TIPO_CHOICES = [
       #      ('urbano','Urbano'),
       #      ('rural', 'Rural')
       #  ]
       #
       #  widgets = {
       #      'matricula_inmobiliaria': forms.TextInput( attrs={'class':'form-control'}),
       #      'cedula_catastral': forms.TextInput( attrs={'class':'form-control'}),
       #      'tipo' : forms.CharField(label="Tipo de predio es:", widget=forms.Select(choices=TIPO_CHOICES)),
       #      'direccion_predio': forms.TextInput( attrs={'class':'form-control'}),
       #      'nombre_predio': forms.TextInput( attrs={'class':'form-control'}),
       #  }
