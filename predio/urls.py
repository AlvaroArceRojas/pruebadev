from django.urls import path
from . import views
from .views import Predio, PredioForm, PredioCreate, PredioDelete,  PredioList, PredioUpdate

app_name = 'predio'
urlpatterns = [
    path('search', views.search, name='predio_filtrar'),
    path('nuevo', PredioCreate.as_view(), name='predio_crear'),
    path('listar', PredioList.as_view(), name= 'predio_listar'),
    path('editar/<int:pk>', PredioUpdate.as_view(), name= 'predio_editar'),
    path('eliminar/<int:pk>', PredioDelete.as_view(), name= 'predio_eliminar'),
]
