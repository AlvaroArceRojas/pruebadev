from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy
from predio.forms import PredioForm
from predio.models import Predio
from django.shortcuts import render
from .filters import PredioFilter


# Create your views here.


def search(request):
    predio_filtrar = Predio.objects.all(cedula_catastral__icontains=request.Get.buscar, tipo__icontains=request.Get.buscar, direccion_predio__icontains=request.Get.buscar, nombre_predio__icontains=request.Get.buscar)
    predio_filter = PredioFilter(request.GET, queryset=predio_filtrar)
    return render(request, 'predio/predio_listar.html', {'filter': predio_filter})


class PredioList(ListView):
    model = Predio
    template_name = 'predio/predio_listar.html'



class PredioCreate(CreateView):
    model = Predio
    form_class = PredioForm
    template_name = 'predio/predio_form.html'
    success_url = reverse_lazy('predio:predio_listar')


class PredioUpdate(UpdateView):
    model = Predio
    form_class = PredioForm
    template_name = 'predio/predio_form.html'
    success_url = reverse_lazy('predio:predio_listar')


class PredioDelete (DeleteView):
    model = Predio
    template_name = 'predio/predio_eliminar.html'
    success_url = reverse_lazy('predio:predio_listar')