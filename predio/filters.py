from predio.models import Predio

class PredioFilter:

    class Meta:
        model = Predio
        fields = ['cedula_catastral', 'tipo', 'direccion_predio', 'nombre_predio']