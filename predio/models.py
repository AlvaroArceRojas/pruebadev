from django.db import models


# Create your models here.

class Predio(models.Model):
    TIPO_CHOICES = (
        ('urbano','Urbano'),
        ('rural','Rural'),
    )
    matricula_inmobiliaria = models.IntegerField(unique=True, primary_key=True, verbose_name='matricula inmobiliaria')
    cedula_catastral = models.CharField(max_length=50, verbose_name='cedula catastral')
    tipo = models.CharField(verbose_name='tipo', max_length=6, choices=TIPO_CHOICES)
    direccion_predio = models.CharField(max_length=255, verbose_name='direccion predio', blank=True, null=True )
    nombre_predio = models.CharField(max_length=128, verbose_name='nombre predio',blank=True , null=True)

    class Meta:
        verbose_name = "predio"
        verbose_name_plural = "predios"
        ordering = ["-matricula_inmobiliaria"]

    def __str__(self):
        return '{}{}{}{}{}'.format(self.matricula_inmobiliaria, self.cedula_catastral, self.tipo,self.direccion_predio, self.nombre_predio)