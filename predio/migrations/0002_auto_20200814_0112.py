# Generated by Django 3.0.8 on 2020-08-14 01:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('predio', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='predio',
            name='direccion_predio',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='direccion predio'),
        ),
        migrations.AlterField(
            model_name='predio',
            name='nombre_predio',
            field=models.CharField(blank=True, max_length=128, null=True, verbose_name='nombre predio'),
        ),
    ]
